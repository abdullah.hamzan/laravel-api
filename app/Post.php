<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Post extends Model
{
    //

    protected $fillable = ['title','id','descriptions','user_id'];
    protected $keyType ="string";
    public $incrementing = false;

    protected static function  boot(){

        parent::boot();



        static::creating(function($model){

            if (empty($model->id)){

                $model->id =Str::uuid();

            }

        });

    }
    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}

<?php

namespace App\Listeners;

use App\Mail\RegenerateMail;
use Illuminate\Support\Facades\Mail;
use App\Events\RegenerateStoredEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToRegenerate implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegenerateStoredEvent  $event
     * @return void
     */
    public function handle(RegenerateStoredEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new RegenerateMail($event->otp_code));
    }
}

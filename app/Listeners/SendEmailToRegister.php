<?php

namespace App\Listeners;

use App\Mail\RegisterMail;
use App\Events\RegisterStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailToRegister implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  RegisterStoredEvent  $event
     * @return void
     */
    public function handle(RegisterStoredEvent $event)
    {
       
        Mail::to($event->otp_code->user->email)->send(new RegisterMail($event->otp_code));
    }
}

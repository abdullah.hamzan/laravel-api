<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    //
    public function index(){

        $role = Role::latest()->get();

        return response()->json([
            "status"=>true,
            "message"=>"List data Role",
            "data"=>$role
        ]);
    }

    public function store(Request $request){

          //set validation
          $validator = Validator::make($request->all(), [
            'name'   => 'required',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $role = Role::create([

            'name'     => $request->name,

        ]);

        //success save to database
        if($role) {

            return response()->json([
                'success' => true,
                'message' => 'Role Created',
                'data'    => $role
            ], 201);

        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Role Failed to Save',
        ], 409);



    }

    public function show($id){
       //find post by ID
       $role = Role::findOrfail($id);

       //make response JSON
       return response()->json([
           'success' => true,
           'message' => 'Detail Data Post',
           'data'    => $role
       ], 200);
    }

    public function update(Request $request, $id){

             //set validation
             $validator = Validator::make($request->all(), [
                'name'   => 'required',

            ]);

               //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        //find role by ID
        $role = Role::findOrFail($id);

        if($role) {

            //update role
            $role->update([
                'name'     => $request->name,

            ]);
            return response()->json([
                'success' => true,
                'message' => 'Role Updated',
                'data'    => $role
            ], 200);

        }

        //data role not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);


    }

    public function destroy($id){
         //find role by ID
         $role = Role::findOrfail($id);

         if($role) {

             //delete role
             $role->delete();
             return response()->json([
                 'success' => true,
                 'message' => 'Role Deleted',
             ], 200);

         }

         //data post not found
         return response()->json([
             'success' => false,
             'message' => 'Role Not Found',
         ], 404);

    }
}

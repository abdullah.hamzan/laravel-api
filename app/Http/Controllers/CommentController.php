<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Events\CommentStoredEvent;
use App\Mail\CommentAuthorMail;
use App\Mail\PostAuthorMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{
    //

    public function index(){

        $comment = Comment::latest()->get();

        return response()->json([
            "status"=>true,
            "message"=>"List data Comment",
            "data"=>$comment
        ]);
    }
    public function show($id){
        //find comment by ID
        $comment = Comment::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data comment',
            'data'    => $comment
        ], 200);
     }



    public function store(Request $request){

        //set validation
        $validator = Validator::make($request->all(), [
          'content'   => 'required',
          'post_id' => 'required',
      ]);

      //response error validation
      if ($validator->fails()) {
          return response()->json($validator->errors(), 400);
      }

      //save to database
      $comment = Comment::create([

          'content'     => $request->content,
          'post_id'   => $request->post_id,

      ]);

      // menggunakan event

      event(new CommentStoredEvent($comment));




      // ini di kirim notification pesan email ke pemilik Post
    //   Mail::to($comment->post->user->email)->send(new PostAuthorMail($comment));

      // ini di kirim notification pesan email ke pemilik comment
    //   Mail::to($comment->user->email)->send(new CommentAuthorMail($comment));

      //success save to database
      if($comment) {

          return response()->json([
              'success' => true,
              'message' => 'Comment Created',
              'data'    => $comment
          ], 201);

      }

      //failed save to database
      return response()->json([
          'success' => false,
          'message' => 'Comment Failed to Save',
      ], 409);



  }

  public function update(Request $request, $id){

    //set validation
    $validator = Validator::make($request->all(), [
       'content'   => 'required',
       'post_id' => 'required',
   ]);

      //response error validation
if ($validator->fails()) {
   return response()->json($validator->errors(), 400);
}


//find comment by ID
$comment = Comment::findOrFail($id);

if($comment) {



   //update post
   $comment->update([
       'content'     => $request->content,
       'post_id'   => $request->post_id
   ]);

   return response()->json([
       'success' => true,
       'message' => 'Comment Updated',
       'data'    => $comment
   ], 200);

}

//data post not found
return response()->json([
   'success' => false,
   'message' => 'Comment Not Found',
], 404);


}


public function destroy($id){
    //find post by ID
    $comment = Comment::findOrfail($id);

    if($comment) {

        //delete comment
        $comment->delete();
        return response()->json([
            'success' => true,
            'message' => 'Comment Deleted',
        ], 200);

    }

    //data post not found
    return response()->json([
        'success' => false,
        'message' => 'Comment Not Found',
    ], 404);

}
}

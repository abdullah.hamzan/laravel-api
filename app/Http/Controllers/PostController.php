<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;



class PostController extends Controller
{
    //

    public function index(){

        $post = Post::latest()->get();

        return response()->json([
            "status"=>true,
            "message"=>"List data Post",
            "data"=>$post
        ]);
    }

    public function store(Request $request){

          //set validation
          $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'descriptions' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $user= auth()->user();

        //save to database
        $post = Post::create([

            'title'     => $request->title,
            'descriptions'   => $request->descriptions,
            'user_id'=>$user->id
        ]);

        //success save to database
        if($post) {



            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data'    => $post
            ], 201);

        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);



    }

    public function show($id){
       //find post by ID
       $post = Post::findOrfail($id);

       //make response JSON
       return response()->json([
           'success' => true,
           'message' => 'Detail Data Post',
           'data'    => $post
       ], 200);
    }

    public function update(Request $request, $id){

             //set validation
             $validator = Validator::make($request->all(), [
                'title'   => 'required',
                'descriptions' => 'required',
            ]);

               //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }


        //find post by ID
        $post = Post::findOrFail($id);

        if($post) {

            $user= auth()->user();

            if ($post->user_id !=$user->id){

                   //failed save to database
             return response()->json([
                'success' => false,
                'message' => 'Data post bukan milik user login',
                'data'=>$post
             ], 403);




            }

            //update post
            $post->update([
                'title'     => $request->title,
                'descriptions'   => $request->descriptions
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data'    => $post
            ], 200);

        }

        //data post not found
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);


    }

    public function destroy($id){
         //find post by ID
         $post = Post::findOrfail($id);

         if($post) {
            $user= auth()->user();
            if ($post->user_id !=$user->id){
                   //failed save to database
             return response()->json([
                'success' => false,
                'message' => 'Data post bukan milik user login',
                'data'=>$post
             ], 403);

            }


             //delete post
             $post->delete();
             return response()->json([
                 'success' => true,
                 'message' => 'Post Deleted',
             ], 200);

         }

         //data post not found
         return response()->json([
             'success' => false,
             'message' => 'Post Not Found',
         ], 404);

    }
}

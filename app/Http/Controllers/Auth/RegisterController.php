<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Events\RegisterStoredEvent;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {

        //   dd('masuk data');
           //set validation
           $validator = Validator::make($request->all(), [
            'name'   => 'required',
            'email' => 'required|unique:users,email|email',
            'username'=>'required|unique:users,username'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $user = User::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'username'=>$request->username,

        ]);

        do {
            # code...
            $random= mt_rand(100000,999999);
            $check =OtpCode::where('otp_code',$random)->first();

        } while ($check);

        $now= Carbon::now();

        $otp_code= OtpCode::create([
            'otp_code'=>$random,
            'valid_until'=>$now->addMinutes(5),
            'user_id'=> $user->id

        ]);

        // menggunakan event
        event(new RegisterStoredEvent($otp_code));




        return response()->json([
            'success'=>true,
            'message'=>'Data user berhasil dibuat',
            'data'=> [
                'user'=>$user,
                'otp_code'=>$otp_code
            ]
            ]);


    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Validator;

class VerificationController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //


           //set validation
           $validator = Validator::make($request->all(), [
            'otp_code'   => 'required',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $otp_code = OtpCode::where('otp_code',$request->otp_code)->first();
        
        if (!$otp_code){
            return response()->json([
                'success'=>false,
                'message'=>'otp code tidak ditemukan',

            ],400);
        }

        $now= Carbon::now();

        if ($now > $otp_code->valid_until){
            return response()->json([
                'success'=>false,
                'message'=>'otp code tidak berlaku',

            ],400);

        }

        $user =User::find($otp_code->user_id);
        $user->update([
            'email_verified'=>now()
        ]);

        $otp_code->delete();


        return response()->json([
            'success'=>true,
            'message'=>'User berhasil di verifikasi',
            'data'=>$user

        ],200);



    }
}

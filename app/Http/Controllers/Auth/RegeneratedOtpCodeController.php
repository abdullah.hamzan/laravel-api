<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\OtpCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Events\RegisterStoredEvent;
use App\Http\Controllers\Controller;
use App\Events\RegenerateStoredEvent;
use Illuminate\Support\Facades\Validator;

class RegeneratedOtpCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
           //set validation
           $validator = Validator::make($request->all(), [
            'email'   => 'required',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email',$request->email)->first();

        if( $user->otp_code){
             // hapus otp_code lama
         $user->otp_code->delete();


        }


        do {
            # code...
            $random= mt_rand(100000,999999);
            $check =OtpCode::where('otp_code',$random)->first();

        } while ($check);

        $now= Carbon::now();

        $otp_code= OtpCode::create([
            'otp_code'=>$random,
            'valid_until'=>$now->addMinutes(5),
            'user_id'=> $user->id

        ]);

             // menggunakan event
        event(new RegenerateStoredEvent($otp_code));



        return response()->json([
            'success'=>true,
            'message'=>'Otp Code berhasil di generated',
            'data'=> [
                'user'=>$user,
                'otp_code'=>$otp_code
            ]
            ]);




    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable implements JWTSubject
{
    use Notifiable;



    protected $fillable = ['username','email','name','role_id','password','email_verified'];
    protected $keyType ="string";
    public $incrementing = false;

    protected static function  boot(){

        parent::boot();


        static::creating(function($model){

            if (empty($model->id)){

                $model->id =Str::uuid();

            }

        });

    }

    public function role()
    {
        return $this->belongsTo('App\Role');
    }
    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }


      // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}

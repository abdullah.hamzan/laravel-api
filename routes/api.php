<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/test', function () {
    return 'selamat datang mas abdul';
});


Route::get('/post', 'PostController@index');
Route::post('/post', 'PostController@store')->middleware('auth:api');
Route::get('/post/{id}', 'PostController@show');
Route::put('/post/{id}', 'PostController@update')->middleware('auth:api');
Route::delete('/post/{id}', 'PostController@destroy')->middleware('auth:api');

// CRUD Comment
Route::get('/comment', 'CommentController@index');
Route::post('/comment', 'CommentController@store')->middleware('auth:api');
Route::get('/comment/{id}', 'CommentController@show');
Route::put('/comment/{id}', 'CommentController@update')->middleware('auth:api');
Route::delete('/comment/{id}', 'CommentController@destroy')->middleware('auth:api');

// CRUD Role
Route::get('/role', 'RoleController@index');
Route::post('/role', 'RoleController@store');
Route::get('/role/{id}', 'RoleController@show');
Route::put('/role/{id}', 'RoleController@update');
Route::delete('/role/{id}', 'RoleController@destroy');


// Auth

Route::group([
    'prefix' => 'auth',
    'namespace' => 'Auth'
], function () {

    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regeneratedOtpCode', 'RegeneratedOtpCodeController')->name('auth.regeneratedOtpCode');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('update_password', 'UpdatePasswordController')->name('auth.update_password');
    Route::post('login', 'LoginController')->name('auth.login');
});
